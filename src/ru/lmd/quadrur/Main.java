package ru.lmd.quadrur;

import java.util.Scanner;

public class Main {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            Quadrur quadrur = new Quadrur(scanner.nextDouble(), scanner.nextDouble(), scanner.nextDouble());
            quadrur.solutionQuadrur();
        } catch (NoSolutionException e) {
             e.printStackTrace();
        }
    }
}
