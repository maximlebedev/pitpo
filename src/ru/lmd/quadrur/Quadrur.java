package ru.lmd.quadrur;

import java.util.ArrayList;

public class Quadrur {

    double sideA, sideB, sideC;

    Quadrur(double sideA, double sideB, double sideC) throws NoSolutionException {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;

        verificationDiscriminant();
    }

    ArrayList<Double> solutionQuadrur() {
        ArrayList<Double> solution = new ArrayList<>();
        if(findDiscriminant() == 0) {
            solution.add(findX());
            return solution;
        } else {
            solution.add(findX());
            solution.add(findSecondX());
            return solution;
        }
    }

    private double findDiscriminant() {
        return sideB * sideB - 4 * sideA * sideC;
    }

    private void verificationDiscriminant() throws NoSolutionException {
        if (findDiscriminant() < 0) {
            throw new NoSolutionException("Корней нет, дискриминант меньше 0");
        }
    }

    private double findX() {
        return ((-sideB + Math.sqrt(findDiscriminant())) / 2 * sideA);
    }

    private double findSecondX() {
        return ((-sideB - Math.sqrt(findDiscriminant())) / 2 * sideA);
    }
}
