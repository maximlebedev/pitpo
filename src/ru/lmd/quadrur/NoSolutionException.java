package ru.lmd.quadrur;

public class NoSolutionException extends Exception {

    NoSolutionException(String message) {
        super(message);
    }

    NoSolutionException() {
    }
}
