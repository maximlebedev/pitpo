package ru.lmd.triangle;

import java.util.Scanner;

public class Main {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        try {
            Triangle triangle = new Triangle(sc.nextInt(), sc.nextInt(), sc.nextInt());

            System.out.println("Равносторонний  - " + triangle.isEquidistant());
            System.out.println("Неравностронний - " + triangle.isScalene());
            System.out.println("Равнобедренный  - " + triangle.isIsosceles());

        } catch (IncorrectSide | IncorrectTriangle e) {
            e.printStackTrace();
        }
    }
}
