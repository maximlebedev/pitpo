package ru.lmd.triangle;

public class IncorrectTriangle extends Exception {

    IncorrectTriangle(String message) {
        super(message);
    }

    IncorrectTriangle() {
    }
}
