package ru.lmd.triangle;

public class IncorrectSide extends Exception {

    IncorrectSide(String message) {
        super(message);
    }

    IncorrectSide() {
    }
}
