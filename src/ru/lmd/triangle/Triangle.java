package ru.lmd.triangle;

public class Triangle {

    private int sideA, sideB, sideC;

    public Triangle(int sideA, int sideB, int sideC) throws IncorrectSide, IncorrectTriangle {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;

        isTriangle();
    }

    private void isTriangle() throws IncorrectTriangle, IncorrectSide {
        if (sideA <= 0 || sideB <= 0 || sideC <= 0) {
            throw new IncorrectSide("Неправильная одна из сторон");
        }

        if (sideA + sideB <= sideC || sideB + sideC <= sideA || sideA + sideC <= sideB) {
            throw new IncorrectTriangle("Неверный треугольник");
        }
    }

    public boolean isEquidistant() { //Проверка на равносторонность
        return sideA == sideB && sideB == sideC;
    }

    public boolean isScalene() {//Проверка на неравносторонность
        return !isEquidistant();
    }

    public boolean isIsosceles() {//Проверка на равнобедренность
        return (sideA == sideB) || (sideB == sideC) || (sideA == sideC);
    }
}
