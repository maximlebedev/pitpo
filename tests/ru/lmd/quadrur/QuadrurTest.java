package ru.lmd.quadrur;

import org.junit.Test;

import static org.junit.Assert.*;

public class QuadrurTest {

    @Test(expected = NoSolutionException.class)
    public void incorrectDiscriminant() throws NoSolutionException {
        Quadrur quadrur = new Quadrur(4, 1, 1);
    }

    @Test
    public void solutionQuadrurZeroDiscriminant() throws NoSolutionException {
        Quadrur quadrur = new Quadrur(1, 4, 4);
    }

    @Test
    public void solutionQuadrurPositiveDiscriminant() throws NoSolutionException {
        Quadrur quadrur = new Quadrur(3, 8, 4);
    }
}