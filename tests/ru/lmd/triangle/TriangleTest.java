package ru.lmd.triangle;

import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TriangleTest {

    @Test(expected = IncorrectTriangle.class)
    public void incorrectTriangle() throws IncorrectSide, IncorrectTriangle {
        new Triangle(2, 2, 4);
    }

    @Test(expected = IncorrectSide.class)
    public void incorrectSide() throws IncorrectSide, IncorrectTriangle {
        new Triangle(-2, 2, 2);
    }

    @Test
    public void isEquidistantTest() throws IncorrectSide, IncorrectTriangle {
        Triangle triangle = new Triangle(3, 3, 3);
        assertTrue(triangle.isEquidistant());
    }

    @Test
    public void isScaleneTest() throws IncorrectSide, IncorrectTriangle {
        Triangle triangle = new Triangle(3, 2, 4);
        assertTrue(triangle.isScalene());
    }

    @Test
    public void isIsoscelesTest() throws IncorrectSide, IncorrectTriangle {
        Triangle triangle = new Triangle(3, 3, 4);
        assertTrue(triangle.isIsosceles());
    }
}